package com.example.dizzer.walletkeeper.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.example.dizzer.walletkeeper.activity.AddExpenses;
import com.example.dizzer.walletkeeper.activity.AddIncomes;

/**
 * Created by Dizzer on 2/18/2018.
 */

public class PickAddOperationDialog extends DialogFragment {

    public PickAddOperationDialog() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Pick operation")
                .setMessage("Please pick what you want to add")
                .setPositiveButton("Incomes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getContext(),AddIncomes.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Expenses", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getContext(),AddExpenses.class);
                        startActivity(intent);
                    }
                });


        return builder.show();
    }
}

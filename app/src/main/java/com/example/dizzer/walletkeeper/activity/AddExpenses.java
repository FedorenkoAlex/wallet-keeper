package com.example.dizzer.walletkeeper.activity;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.dizzer.walletkeeper.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_EXPENSES;
import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_INCOMES;

public class AddExpenses extends AppCompatActivity {

    @BindView(R.id.et_expenses_label)
    EditText etExpensesLabel;
    @BindView(R.id.et_expenses_sum)
    EditText etExpensesSum;
    @BindView(R.id.et_expenses_date)
    EditText etExpensesDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expenses);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_add_expenses)
    public void addExpenses(View view){
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",etExpensesLabel.getText().toString());
        contentValues.put("price",etExpensesSum.getText().toString());
        getContentResolver().insert(CONTENT_URI_EXPENSES,contentValues);
        onBackPressed();
    }
}

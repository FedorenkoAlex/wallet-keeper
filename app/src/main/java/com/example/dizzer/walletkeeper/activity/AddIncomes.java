package com.example.dizzer.walletkeeper.activity;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.dizzer.walletkeeper.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_INCOMES;

public class AddIncomes extends AppCompatActivity {

    @BindView(R.id.et_incomes_label)
    EditText etIncomesLabel;
    @BindView(R.id.et_incomes_sum)
    EditText etIncomesSum;
    @BindView(R.id.et_incomes_date)
    EditText etIncomesDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_incomes);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_add_incomes)
    public void addIncomes(View view){
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",etIncomesLabel.getText().toString());
        contentValues.put("price",etIncomesSum.getText().toString());
        getContentResolver().insert(CONTENT_URI_INCOMES,contentValues);
        onBackPressed();
    }
}

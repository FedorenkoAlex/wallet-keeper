package com.example.dizzer.walletkeeper.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dizzer.walletkeeper.R;
import com.example.dizzer.walletkeeper.model.Expenses;
import com.example.dizzer.walletkeeper.model.Incomes;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class CustomExpensesRecyclerViewAdapter extends RecyclerView.Adapter<CustomExpensesRecyclerViewAdapter.ViewHolder> {

    private List<Expenses> expenses;

    public CustomExpensesRecyclerViewAdapter(List<Expenses> expenses) {
        this.expenses = expenses;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_expenses_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvAddExpensesFragmentName.setText(expenses.get(position).getName());
        holder.tvAddExpensesFragmentSum.setText(String.valueOf(expenses.get(position).getSum()));
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.tv_add_expenses_fragment_sum)
        TextView tvAddExpensesFragmentSum;
        @BindView(R.id.tv_add_expenses_fragment_name)
        TextView tvAddExpensesFragmentName;
        @BindView(R.id.tv_add_expenses_fragment_date)
        TextView tvAddExpensesFragmentDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }

        @Override
        public void onClick(View view) {
            int itemPosition = getLayoutPosition();
        }
    }
}

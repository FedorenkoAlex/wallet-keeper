package com.example.dizzer.walletkeeper.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.dizzer.walletkeeper.R;
import com.example.dizzer.walletkeeper.model.Incomes;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class CustomIncomesRecyclerViewAdapter extends RecyclerView.Adapter<CustomIncomesRecyclerViewAdapter.ViewHolder> {

    private List<Incomes> incomes;

    public CustomIncomesRecyclerViewAdapter(List<Incomes> incomes) {
        this.incomes = incomes;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_incomes_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvAddIncomesFragmentName.setText(incomes.get(position).getName());
        holder.tvAddIncomesFragmentSum.setText(String.valueOf(incomes.get(position).getSum()));
    }

    @Override
    public int getItemCount() {
        return incomes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.tv_add_incomes_fragment_sum)
        TextView tvAddIncomesFragmentSum;
        @BindView(R.id.tv_add_incomes_fragment_name)
        TextView tvAddIncomesFragmentName;
        @BindView(R.id.tv_add_incomes_fragment_date)
        TextView tvAddIncomesFragmentDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }

        @Override
        public void onClick(View view) {
            int itemPosition = getLayoutPosition();
        }
    }
}

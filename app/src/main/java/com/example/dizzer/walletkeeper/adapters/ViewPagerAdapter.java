package com.example.dizzer.walletkeeper.adapters;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.dizzer.walletkeeper.R;
import com.example.dizzer.walletkeeper.fragments.ExpensesFragment;
import com.example.dizzer.walletkeeper.fragments.IncomesFragment;
import com.example.dizzer.walletkeeper.fragments.StatisticsFragment;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private int pageCount;

    public ViewPagerAdapter(FragmentManager fm, Context context, int pageCount) {
        super(fm);
        this.context = context;
        this.pageCount = pageCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new StatisticsFragment();
            case 1 :
                return new IncomesFragment();
            case 2 :
                return new ExpensesFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return pageCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0 :
                return context.getString(R.string.statistics);
            case 1 :
                return context.getString(R.string.incomes);
            case 2 :
                return context.getString(R.string.expenses);
            default:
                return null;
        }
    }
}

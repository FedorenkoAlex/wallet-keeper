package com.example.dizzer.walletkeeper.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dizzer.walletkeeper.R;
import com.example.dizzer.walletkeeper.adapters.CustomExpensesRecyclerViewAdapter;
import com.example.dizzer.walletkeeper.model.Expenses;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_EXPENSES;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class ExpensesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    @BindView(R.id.expenses_fragment_expenses)
    TextView expensesFragmentExpenses;
    @BindView(R.id.expenses_fragment_recycler_view)
    RecyclerView expensesFragmentRecyclerView;
    Unbinder unbinder;

    private CustomExpensesRecyclerViewAdapter mCustomExpensesRecyclerViewAdapter;
    private List<Expenses> expenses;
    private String currentExpensesSum = "0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.expenses_layout, container, false);
        unbinder = ButterKnife.bind(this, view);

        expensesFragmentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setRecyclerView();

        getLoaderManager().initLoader(1,null,this);

        return view;
    }

    private void setRecyclerView() {
        mCustomExpensesRecyclerViewAdapter = new CustomExpensesRecyclerViewAdapter(getList());
        expensesFragmentExpenses.setText(getSum());
        expensesFragmentRecyclerView.setAdapter(mCustomExpensesRecyclerViewAdapter);
    }

    private String getSum(){

        String[] sum = {"SUM(price)"};
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_EXPENSES, sum, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            currentExpensesSum = cursor.getString(cursor.getColumnIndex("SUM(price)"));
            cursor.close();
        }

        return currentExpensesSum;
    }

    private List<Expenses> getList() {
        expenses = new ArrayList<>();
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_EXPENSES, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {
                long expensesId = cursor.getLong(cursor.getColumnIndex("_id"));
                String expensesName = cursor.getString(cursor.getColumnIndex("name"));
                long expensesSum = cursor.getLong(cursor.getColumnIndex("price"));

                Expenses expense = new Expenses(expensesId, expensesName, expensesSum);
                expenses.add(expense);

            } while (cursor.moveToNext());

            cursor.close();
        }

        return expenses;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CONTENT_URI_EXPENSES, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setRecyclerView();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

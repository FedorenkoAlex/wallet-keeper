package com.example.dizzer.walletkeeper.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dizzer.walletkeeper.R;
import com.example.dizzer.walletkeeper.adapters.CustomIncomesRecyclerViewAdapter;
import com.example.dizzer.walletkeeper.model.Incomes;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_INCOMES;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class IncomesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    @BindView(R.id.incomes_fragment_incomes)
    TextView incomesFragmentIncomes;
    @BindView(R.id.incomes_fragment_recycler_view)
    RecyclerView incomesFragmentRecyclerView;
    Unbinder unbinder;

    private CustomIncomesRecyclerViewAdapter mCustomIncomesRecyclerViewAdapter;
    private List<Incomes> incomes;
    private String currentIncomesSum = "0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.incomes_layout, container, false);
        unbinder = ButterKnife.bind(this, view);

        incomesFragmentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setRecyclerView();

        getLoaderManager().initLoader(1,null,this);

        return view;
    }

    private void setRecyclerView() {
        mCustomIncomesRecyclerViewAdapter = new CustomIncomesRecyclerViewAdapter(getList());
        incomesFragmentIncomes.setText(getSum());
        incomesFragmentRecyclerView.setAdapter(mCustomIncomesRecyclerViewAdapter);
    }

    private String getSum(){

        String[] sum = {"SUM(price)"};
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_INCOMES, sum, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            currentIncomesSum = cursor.getString(cursor.getColumnIndex("SUM(price)"));
            cursor.close();
        }

        return currentIncomesSum;
    }

    private List<Incomes> getList() {
        incomes = new ArrayList<>();
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_INCOMES, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {
                long incomeId = cursor.getLong(cursor.getColumnIndex("_id"));
                String incomeName = cursor.getString(cursor.getColumnIndex("name"));
                long incomeSum = cursor.getLong(cursor.getColumnIndex("price"));

                Incomes income = new Incomes(incomeId, incomeName, incomeSum);
                incomes.add(income);

            } while (cursor.moveToNext());

            cursor.close();
        }

        return incomes;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CONTENT_URI_INCOMES, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setRecyclerView();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

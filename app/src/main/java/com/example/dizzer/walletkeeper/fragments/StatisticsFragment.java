package com.example.dizzer.walletkeeper.fragments;

import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dizzer.walletkeeper.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_EXPENSES;
import static com.example.dizzer.walletkeeper.provider.CustomContentProvider.CONTENT_URI_INCOMES;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class StatisticsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    @BindView(R.id.statistics_fragment_incomes)
    TextView statisticsFragmentIncomes;
    @BindView(R.id.statistics_fragment_expenses)
    TextView statisticsFragmentExpenses;
    Unbinder unbinder;

    private String incomesSum = "0";
    private String expensesSum = "0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statistics_layout, container, false);
        unbinder = ButterKnife.bind(this, view);

        getLoaderManager().initLoader(1,null,this);
        getLoaderManager().initLoader(2,null,this);

        return view;
    }

    private String getIncomesSum(){

        String[] sum = {"SUM(price)"};
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_INCOMES, sum, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            incomesSum = cursor.getString(cursor.getColumnIndex("SUM(price)"));
            cursor.close();
        }

        return incomesSum;
    }

    private String getExpensesSum(){

        String[] sum = {"SUM(price)"};
        Cursor cursor = getActivity().getContentResolver().query(CONTENT_URI_EXPENSES, sum, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            expensesSum = cursor.getString(cursor.getColumnIndex("SUM(price)"));
            cursor.close();
        } else {
            expensesSum = "0";
        }

        return expensesSum;
    }

    private void setSum(){
        statisticsFragmentIncomes.setText(getIncomesSum());
        statisticsFragmentExpenses.setText(getExpensesSum());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case 1:
                return new CursorLoader(getActivity(), CONTENT_URI_INCOMES, null, null, null, null);
            case 2:
                return new CursorLoader(getActivity(), CONTENT_URI_EXPENSES, null, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        setSum();
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {

    }
}

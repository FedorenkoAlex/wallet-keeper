package com.example.dizzer.walletkeeper.model;

import java.util.Date;

/**
 * Created by Dizzer on 1/25/2018.
 */

public class Incomes {

    private long id;
    private String name;
    private Date date;
    private long sum;

    public Incomes(long id, String name, long sum) {
        this.id = id;
        this.name = name;
        this.sum = sum;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getSum() {
        return sum;
    }
}

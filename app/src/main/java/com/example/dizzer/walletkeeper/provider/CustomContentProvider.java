package com.example.dizzer.walletkeeper.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Dizzer on 1/27/2018.
 */

public class CustomContentProvider extends ContentProvider {

    public static final String CONTENT_AUTORITY = "com.example.dizzer.walletkeeper.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTORITY);
    private static final String INCOMES_PATH = "incomes";
    private static final String EXPENSES_PATH = "expenses";
    public static final Uri CONTENT_URI_INCOMES = BASE_CONTENT_URI.buildUpon().appendPath(INCOMES_PATH).build();
    public static final Uri CONTENT_URI_EXPENSES = BASE_CONTENT_URI.buildUpon().appendPath(EXPENSES_PATH).build();

    private static final int INCOMES = 1001;
    private static final int EXPENSES = 2001;

    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(CONTENT_AUTORITY, INCOMES_PATH, INCOMES);
        uriMatcher.addURI(CONTENT_AUTORITY, EXPENSES_PATH, EXPENSES);
    }

    private CustomDB mCustomDB;

    @Override
    public boolean onCreate() {
        mCustomDB = new CustomDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        SQLiteDatabase database = mCustomDB.getReadableDatabase();
        int mUriMatcher = uriMatcher.match(uri);
        Cursor cursor;
        Context context = getContext();
        assert context != null;
        switch (mUriMatcher){
            case INCOMES :
                cursor = database.query(CustomDB.INCOMES_TABLE_NAME, strings, s, strings1, null, null, s1);
                cursor.setNotificationUri(context.getContentResolver(), uri);
                return cursor;
            case EXPENSES :
                cursor = database.query(CustomDB.EXPENSES_TABLE_NAME, strings, s, strings1, null, null, s1);
                cursor.setNotificationUri(context.getContentResolver(), uri);
                return cursor;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase database = mCustomDB.getWritableDatabase();
        int mUriMatcher = uriMatcher.match(uri);
        Uri resultUri;
        Context context = getContext();
        assert context != null;
        switch (mUriMatcher){
            case INCOMES :
                long insertIncomes = database.insert(CustomDB.INCOMES_TABLE_NAME, null, contentValues);
                resultUri = ContentUris.withAppendedId(uri, insertIncomes);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultUri;
            case EXPENSES :
                long insertExpenses = database.insert(CustomDB.EXPENSES_TABLE_NAME, null, contentValues);
                resultUri = ContentUris.withAppendedId(uri, insertExpenses);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultUri;
            default:
                return null;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    private static class CustomDB extends SQLiteOpenHelper{

        private static final String DB_NAME = "Wallet.db";
        private static final int DB_VERSION = 1;

        private static final String INCOMES_TABLE_NAME = "incomes";
        private static final String EXPENSES_TABLE_NAME = "expenses";

        private static final String TABLE_ID = "_id";
        private static final String TABLE_NAME = "name";
        private static final String TABLE_PRICE = "price";
        private static final String TABLE_DATE = "date";
        private static final String TABLE_CATEGORY = "category";

        private static final String SQL_CREATE_TABLE_INCOMES = "CREATE TABLE IF NOT EXISTS " + INCOMES_TABLE_NAME
                + " ("
                + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TABLE_NAME + " TEXT,"
                + TABLE_PRICE + " DOUBLE,"
                + TABLE_DATE + " DATE)";

        private static final String SQL_CREATE_TABLE_EXPENSES = "CREATE TABLE IF NOT EXISTS " + EXPENSES_TABLE_NAME
                + " ("
                + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TABLE_NAME + " TEXT,"
                + TABLE_CATEGORY + " TEXT,"
                + TABLE_PRICE + " DOUBLE,"
                + TABLE_DATE + " DATE)";

        private static final String SQL_DROP_TABLE_INCOMES = "DROP TABLE " + INCOMES_TABLE_NAME;
        private static final String SQL_DROP_TABLE_EXPENSES = "DROP TABLE " + EXPENSES_TABLE_NAME;

        public CustomDB(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(SQL_CREATE_TABLE_INCOMES);
            sqLiteDatabase.execSQL(SQL_CREATE_TABLE_EXPENSES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(SQL_DROP_TABLE_INCOMES);
            sqLiteDatabase.execSQL(SQL_DROP_TABLE_EXPENSES);
            onCreate(sqLiteDatabase);

        }
    }
}
